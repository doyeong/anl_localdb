# ANL_localDB Instantiation

![ANL LocalDB schematic](localdb_schematic.png)


We have a computer named 'cleanroom' which is the localDB host machine at AMAF. This computer host local DB and mongo DB and all ANL's local data will be stored in this single computer.
The localDB computer is on the HEP private network, and all DAQ computers including metrologi computer can access to the server computer as long as they are also on the same network.
Each DAQ computer will control two testboxes which has a Raspberry Pi for DCS. Grafana, infulxDB, YARR, and other QC tools should be installed separately in individual DAQ computer. 
We'll have 6 DAQ computers in total during production time.

## Create volumes for data storage

```
mkdir /data/influxdb
mkdir /data/grafana
mkdir /data/mongodb
mkdir /data/prometheus
```

## Create network for web

```
docker network create web
```

## Run in Daemon Mode

```
docker-compose up -d
```


## How to rerun 

```
sudo docker-compose down
sudo docker-compose up -d
```

## How to check log

```
sudo docker ps
sudo docker logs <CONTAINER ID>
```

## Create admin account
Execute mongodb
```
sudo docker exec -it mongodb bash
mongosh --host 127.0.0.1 --port 27017
```

From mongodb shell
```
use localdb
localdb> db.createUser({user: 'admin', pwd: 't3stb3am!',roles: [{role: 'readWrite', db: 'localdb'},{role: 'readWrite', db: 'localdbtools'},{role: 'userAdmin', db: 'localdb'},{role: 'userAdmin', db: 'localdbtools'}]})
localdb> db.createUser({user: '21232f297a57a5a743894a0e4a801fc3', pwd: 'b45beff13ea5adadd10ac7a91834f1ff', roles: [{role: 'readWrite', db: 'localdb'},{role: 'readWrite', db: 'localdbtools'},{role: 'userAdmin', db:'localdb'},{role: 'userAdmin', db: 'localdbtools'}]})
use localdbtools
db.dropUser('admin')
```

where, the user and password for the second line have to be hashed via md5
```
[cleanroom@localhost localDB_V2.0.4]$ printf 'admin' | md5sum
21232f297a57a5a743894a0e4a801fc3  -
[cleanroom@localhost localDB_V2.0.4]$ printf 't3stb3am!' | md5sum
b45beff13ea5adadd10ac7a91834f1ff  -
```

# Troubleshooting
## received 504 HTTP response
Check if this site is working: 
https://itkpd2eos.unicornuniversity.net/
